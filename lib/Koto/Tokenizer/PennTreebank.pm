package Koto::Tokenizer::PennTreebank {
	use Moose;

	extends 'Koto::Tokenizer';

	has 'lowercase' => (
		is => 'rw',
		isa => 'Bool',
		default => 0,
	);

	sub tokenize {
		my ( $self, $text ) = @_;

		# --- Starting quotes
		$text =~ s/^\"/``/g;
		$text =~ s/(``)/ $1 /g;
		$text =~ s/([ (\[{<])"/$1 `` /g;

		# --- Punctuation
		$text =~ s/([:,])([^\d])/ $1 $2/;
		$text =~ s/([:,])$/ $1 /;
		$text =~ s/\.\.\./ ... /;
		$text =~ s/[;@#$%&]/ $& /;
		$text =~ s/([^\.])(\.)([\]\)}>"\']*)\s*$/$1 $2$3 /;
		$text =~ s/[?!]/ $& /;
		$text =~ s/([^'])' /$1 ' /;

		# --- Brackets
		$text =~ s/[\]\[\(\)\{\}\<\>]/ $& /;
		$text =~ s/--/ -- /;

		$text = sprintf( ' %s ', $text );

		# --- Ending quotes
		$text =~ s/"/ '' /;
		$text =~ s/(\S)(\'\')/$1 $2 /;
		$text =~ s/([^' ])('[sS]|'[mM]|'[dD]|') /$1 $2 /;
		$text =~ s/([^' ])('ll|'LL|'re|'RE|'ve|'VE|n't|N'T) /$1 $2 /;

		# --- Contractions
		$text =~ s/\b(can)(not)\b/ $1 $2 /gi;
		$text =~ s/\b(d)('ye)\b/ $1 $2 /gi;
		$text =~ s/\b(gim)(me)\b/ $1 $2 /gi;
		$text =~ s/\b(gon)(na)\b/ $1 $2 /gi;
		$text =~ s/\b(got)(ta)\b/ $1 $2 /gi;
		$text =~ s/\b(lem)(me)\b/ $1 $2 /gi;
		$text =~ s/\b(more)('n)\b/ $1 $2 /gi;
		$text =~ s/\b(wan)(na) / $1 $2 /gi;
		$text =~ s/\ ('t)(is)\b/ $1 $2 /gi;
		$text =~ s/\ ('t)(was)\b/ $1 $2 /gi;

		# ---

		# $text =~ s/\ \ +/ /g;
		# $text =~ s/^\ |\ $//g;

		return split( m/\s/, $text );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
