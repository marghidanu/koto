package Koto::Utils {
	use Moose;
	use Moose::Exporter;

	use File::ShareDir;

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( dist_dir ) ]
	);

	sub dist_dir { File::ShareDir::dist_dir( 'Koto' ) }

	__PACKAGE__->meta()->make_immutable();
}

1;
