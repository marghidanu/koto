#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

require_ok( 'Koto::Document' );

my $doc = Koto::Document->new(
	{
		text => 'The quick brown fox jumped over the lazy brown dog'
	}
);

isa_ok( $doc, 'Koto::Document' );
can_ok( $doc, qw( tf  ) );

use Data::Dumper;
warn( Dumper( $doc->tf() ) );

done_testing();
