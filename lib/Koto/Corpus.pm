package Koto::Corpus {
	use Moose;

	use List::MoreUtils qw( uniq );

	use Koto::Document;

	use File::Find::Rule;
	use File::Slurp qw( read_file );

	has 'documents' => (
		is => 'ro',
		isa => 'ArrayRef[Koto::Document]',
		default => sub { [] },
		traits => [ qw( Array ) ],
		handles => {
			'total_documents' => 'count',
			'all_documents' => 'elements',
			'add_document' => 'push',
		},
	);

	sub add_raw_document { shift()->add_document( Koto::Document->new( shift() || {} ) ) }

	sub load_directory {
		my ( $class, $directory ) = @_;

		my $corpus = $class->new();

		File::Find::Rule->file()
			->not_empty()
			->exec(
				sub {
					my ( $short_name, $path, $full_name ) = @_;

					my $text = read_file( $full_name );
					$corpus->add_raw_document(
						{
							text => $text,
							metadata => {
								file => $full_name,
							}
						}
					)
				}
			)
			->in( $directory );

		return $corpus;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
