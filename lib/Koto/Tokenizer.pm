package Koto::Tokenizer {
	use Moose;

	use MooseX::AbstractMethod;

	abstract( 'tokenize' );

	__PACKAGE__->meta()->make_immutable();
}

1;
