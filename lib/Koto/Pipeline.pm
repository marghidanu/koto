package Koto::Pipeline {
	use Moose;

	use Scalar::Util qw( blessed );

	has 'stages' => (
		is => 'ro',
		isa => 'ArrayRef[Koto::Pipeline::Stage|CodeRef]',
		default => sub { [] },
		traits => [ qw( Array ) ],
		handles => {
			'all_stages' => 'elements',
			'add_stage' => 'push'
		}
	);

	sub run {
		my ( $self, $args ) = @_;

		my $context = Koto::Pipeline::Context->new( { data => $args || {} } );

		foreach my $stage ( $self->all_stages() ) {
			my $result = blessed( $stage ) ?
				$stage->run( $context ) :
				$stage->( $context );

			last()
				if( blessed( $result ) eq 'Koto::Pipeline::Exit' );
		}

		return $context;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
