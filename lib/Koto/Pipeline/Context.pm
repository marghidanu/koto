package Koto::Pipeline::Context {
	use Moose;

	has 'data' => (
		is => 'ro',
		isa => 'HashRef',
		lazy => 1,
		default => sub { {} },
		traits => [ qw( Hash ) ],
		handles => {
			'set' => 'set',
			'get' => 'get',
		}
	);

	__PACKAGE__->meta()->make_immutable();
}

1;
