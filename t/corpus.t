#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

require_ok( 'Koto::Corpus' );

my $corpus = Koto::Corpus->new();
isa_ok( $corpus, 'Koto::Corpus' );
can_ok( $corpus, qw( add_raw_document get_vocabulary ) );

$corpus->add_raw_document( { text => 'The quick brown fox.' } );
$corpus->add_raw_document( { text => 'Jumped over the lazy dog.' } );
$corpus->add_raw_document( { text => 'On a cold winter day!' } );

my $vocabulary = $corpus->get_vocabulary();

use Data::Dumper;
warn( Dumper( $vocabulary ) );

done_testing();
