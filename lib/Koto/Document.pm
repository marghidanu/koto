package Koto::Document {
	use Moose;

	use Data::UUID;

	has 'id' => (
		is => 'ro',
		isa => 'Str',
		lazy => 1,
		default => sub {
			Data::UUID->new()
				->create_str();
		},
	);

	has 'text' => (
		is => 'rw',
		isa => 'Maybe[Str]',
		required => 1,
	);

	has 'metadata' => (
		is => 'ro',
		isa => 'HashRef',
		default => sub { {} },
	);

	__PACKAGE__->meta()->make_immutable();
}

1;
