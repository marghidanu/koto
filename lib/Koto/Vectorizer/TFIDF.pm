package Koto::Vectorizer::TFIDF {
	use Moose;

	use Koto::TokenizerFactory;

	use List::MoreUtils qw( uniq );

	has 'tokenizer' => (
		is => 'ro',
		isa => 'Koto::Tokenizer',
		lazy => 1,
		default => sub {
			return Koto::TokenizerFactory->create(
				'PennTreebank',
				{}
			);
		}
	);

	sub tf {
		my ( $self, $document ) = @_;

		my @tokens = $self->tokenizer()
			->tokenize( $document->text() );

		my $freq = {};
		$freq->{ $_ }++
			foreach( @tokens );

		return $freq;
	}

	sub idf {
		my ( $self, $corpus ) = @_;

		my $n = $corpus->total_documents();

		my @tfs = map { $self->tf( $_ ) }
			$corpus->all_documents();

		# Document frequency
		my $freq = {};
		$freq->{ $_ }++
			foreach( map { keys( %{ $_ } ) } @tfs );

		return { map { $_ => log( $n/( 1 + $freq->{ $_ } ) ) } keys( %{ $freq } ) }
	}

	sub tf_idf {
		my ( $self, $corpus ) = @_;

		my $idfs = $self->idf( $corpus );

		foreach my $document ( $corpus->all_documents ) {
			my $tf = $self->tf( $document );

			$document->metadata()->{tf_idf} = {
				map { $_ => $tf->{ $_ } * $idfs->{ $_ } }
					keys( %{ $tf } )
			};
		}
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
