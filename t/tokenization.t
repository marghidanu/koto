#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

use Data::Dumper;

require_ok( 'Koto::TokenizerFactory' );

my $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

my $tokenizer = Koto::TokenizerFactory->create(
	'PennTreebank',
	{}
);

my @tokens = $tokenizer->tokenize( $text );
warn( Dumper( \@tokens ) );

done_testing();
