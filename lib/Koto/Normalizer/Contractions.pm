package Koto::Normalizer::Contractions {
	use Moose;

	use Koto::Language::English;

	has '_rule' => (
		is => 'ro',
		isa => 'RegexpRef',
		default => sub {
			my $value = join( '|', keys( $Koto::Language::English::CONTRACTIONS ) );

			return qr/($value)/;
		}
	);

	sub expand {
		my ( $self, $text ) = @_;

		my $rule = $self->_rule();
		$text =~ s/$rule/$Koto::Language::English::CONTRACTIONS->{ $1 }/gi;

		return $text;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
