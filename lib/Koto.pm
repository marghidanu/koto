package Koto {
	use Moose;

	our $VERSION = '1.0';

	__PACKAGE__->meta()->make_immutable();
}

1;
