#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

require_ok( 'Koto::Normalizer::Contractions' );

my $instance = Koto::Normalizer::Contractions->new();
isa_ok( $instance, 'Koto::Normalizer::Contractions' );
can_ok( $instance, qw( expand ) );

is( $instance->expand( "I'm on the yellow zebra" ), 'I am on the yellow zebra' );
is( $instance->expand( "I'll listen to y'all" ), 'I will listen to you all' );
is( $instance->expand( "don't make it right" ), 'do not make it right' );
is( $instance->expand( "What's up" ), 'what is up' );
is( $instance->expand( "you shouldn't have" ), 'you should not have' );
is( $instance->expand( 'you should go' ), 'you should go' );

done_testing();
