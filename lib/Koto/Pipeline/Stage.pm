package Koto::Pipeline::Stage {
	use Moose;

	use MooseX::AbstractMethod;

	use  Koto::Pipeline::Exit;

	abstract( 'run' );

	sub exit() { Koto::Pipeline::Exit->new() }

	__PACKAGE__->meta()->make_immutable();
}

1;
