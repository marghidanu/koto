#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

use Koto::Utils qw( dist_dir );

require_ok( 'Koto::Corpus' );
require_ok( 'Koto::Vectorizer::TFIDF' );

my $corpus = Koto::Corpus->load_directory(
	sprintf( '%s/data/test', dist_dir() )
);

my $vectorizer = Koto::Vectorizer::TFIDF->new();
$vectorizer->tf_idf( $corpus );

use Data::Dumper;
warn( Dumper( $corpus ) );

done_testing();

__DATA__
