package Koto::Vectorizer::TFIDF {
	use Moose;

	use Koto::TokenizerFactory;

	use List::MoreUtils qw( uniq );

	has 'tokenizer' => (
		is => 'ro',
		isa => 'Koto::Tokenizer',
		lazy => 1,
		default => sub {
			return Koto::TokenizerFactory->create(
				'PennTreebank',
				{}
			);
		}
	);

	sub tf {
		my ( $self, $document ) = @_;

		my @tokens = $self->tokenizer()
			->tokenize( $document->text() );

		my $freq = {};
		$freq->{ $_ }++
			foreach( @tokens );

		return $freq;
	}

	sub idf {
		my ( $self, $corpus ) = @_;

		my $n = $corpus->total_documents();

		my @tfs = map { $self->tf( $_ ) }
			$corpus->all_documents();

		my $freq = {};
		$freq->{ $_ }++
			foreach( map { keys( $_ ) } @tfs );

		return { map { $_ => log( $n/( 1 + $freq->{ $_ } ) ) } keys( $freq )  }
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
