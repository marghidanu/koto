package Koto::Tokenizer::Whitespace {
	use Moose;

	extends 'Koto::Tokenizer';

	sub tokenize {
		my ( $self, $text ) = @_;

		return split( m/\s+/, lc( $text ) );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
