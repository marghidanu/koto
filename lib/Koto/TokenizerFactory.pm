package Koto::TokenizerFactory {
	use MooseX::AbstractFactory;

	implementation_class_via sub { sprintf( 'Koto::Tokenizer::%s', shift() ) };
}

1;
